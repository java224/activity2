package com.enriquez;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner appScanner = new Scanner(System.in);
        int yearInput;

        System.out.print("Please Enter a Year to be analyze if it's a leaf year");
        yearInput = appScanner.nextInt();

        if (yearInput % 4 == 0 && yearInput % 100 != 0 || yearInput % 400 == 0)
            System.out.print("Leaf Year");
        else
            System.out.print("Not a Leaf Year");

    }
}
